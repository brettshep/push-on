import {
  Directive,
  ElementRef,
  HostListener,
  Output,
  Input,
  EventEmitter,
} from "@angular/core";

@Directive({
  selector: "[slide]",
})
export class SlideDirective {
  constructor(private elem: ElementRef) {}

  xDown = null;
  thresh = 10;
  swipped: boolean;
  @Input() canSlide: boolean;
  @Output() swipeLeft = new EventEmitter();
  @Output() swipeRight = new EventEmitter();

  @HostListener("mousedown", ["$event"])
  mouseDown(e: MouseEvent) {
    if (!this.canSlide) {
      return;
    }
    this.xDown = e.clientX;
  }
  @HostListener("mousemove", ["$event"])
  mouseMove(e: MouseEvent) {
    if (!this.canSlide) return;
    if (!this.xDown) return;

    const xCurr = e.clientX;
    const xDiff = this.xDown - xCurr;

    if (Math.abs(xDiff) > this.thresh) {
      this.swipped = true;
      Math.sign(xDiff) === -1 ? this.swipeLeft.emit() : this.swipeRight.emit();
    }
  }
  @HostListener("mouseout", ["$event"])
  @HostListener("mouseup", ["$event"])
  mouseUp(e: MouseEvent) {
    if (!this.canSlide) return;

    // capture click
    if (this.swipped) {
      const func = (e: Event) => {
        e.stopPropagation();
        window.removeEventListener("click", func, true);
      };
      window.addEventListener("click", func, true);
    }
    this.swipped = false;
    this.xDown = null;
    e.stopPropagation();
  }

  @HostListener("touchstart", ["$event"])
  touchDown(e: TouchEvent) {
    if (!this.canSlide) return;
    this.xDown = e.touches[0].clientX;
  }
  @HostListener("touchmove", ["$event"])
  touchMove(e: TouchEvent) {
    if (!this.canSlide) return;
    if (!this.xDown) return;

    const xCurr = e.touches[0].clientX;
    const xDiff = this.xDown - xCurr;

    if (Math.abs(xDiff) > this.thresh) {
      Math.sign(xDiff) === -1 ? this.swipeLeft.emit() : this.swipeRight.emit();
    }
  }
  @HostListener("touchleave", ["$event"])
  @HostListener("touchup", ["$event"])
  touchUp(e: TouchEvent) {
    if (!this.canSlide) return;
    this.xDown = null;
  }
}
