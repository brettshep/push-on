import { Component, Output, EventEmitter } from "@angular/core";
import { Habit } from "../../../../interfaces";

@Component({
  selector: "new-habit",
  template: `
    <div class="container">
      <div class="max">
        <h2>New Habit <i class="fas fa-times" (click)="exit.emit()"></i></h2>

        <!-- title -->
        <section>
          TITLE
          <input
            #title
            type="text"
            placeholder="My New Habit..."
            [class.error]="inputError"
            (input)="getInputError(title.value)"
          />
        </section>

        <!-- icon -->
        <section>
          ICON
          <button
            class="emoji-input"
            (click)="openEmoji()"
            [ngClass]="{
              focus: emojiModalOpen,
              error: emojiError
            }"
          >
            <div
              class="emoji-cont"
              *ngIf="emoji; else plus"
              [innerHTML]="emoji | emoji"
            ></div>
            <ng-template #plus><i class="fas fa-plus"></i></ng-template>
          </button>
        </section>

        <!-- Scheduled Days -->
        <section>
          SCHEDULED DAYS
          <div class="days">
            <button (click)="dayChange(0)" [class.active]="activeDays[0]">
              S
            </button>
            <button (click)="dayChange(1)" [class.active]="activeDays[1]">
              M
            </button>
            <button (click)="dayChange(2)" [class.active]="activeDays[2]">
              T
            </button>
            <button (click)="dayChange(3)" [class.active]="activeDays[3]">
              W
            </button>
            <button (click)="dayChange(4)" [class.active]="activeDays[4]">
              T
            </button>
            <button (click)="dayChange(5)" [class.active]="activeDays[5]">
              F
            </button>
            <button (click)="dayChange(6)" [class.active]="activeDays[6]">
              S
            </button>
          </div>
        </section>

        <!-- add button -->
        <button class="submit-btn" (click)="submit(title.value)">
          Add Habit
        </button>

        <!-- emoji modal -->
        <emoji-keyboard
          *ngIf="emojiModalOpen"
          (exit)="emojiModalOpen = false"
          (emoji)="setEmoji($event)"
        ></emoji-keyboard>
      </div>
    </div>
  `,
  styleUrls: ["./new-habit.component.sass"],
})
export class NewHabitComponent {
  @Output() exit = new EventEmitter();
  @Output() habit = new EventEmitter<Habit>();
  emoji: string = null;
  activeDays: boolean[] = [true, true, true, true, true, true, true];

  emojiModalOpen: boolean;
  submitted: boolean;
  inputError: boolean;
  emojiError: boolean;

  submit(title: string) {
    this.submitted = true;
    if (!title) {
      this.inputError = true;
    }
    if (!this.emoji) {
      this.emojiError = true;
    }

    if (title && this.emoji) {
      const habit: Habit = {
        title,
        icon: this.emoji,
        days: this.activeDays
          .map((bool, i) => (bool ? i.toString() : null))
          .filter((val) => val !== null),
        lastChecked: null,
        prevLastChecked: null,
        streak: 0,
        created: Date.now(),
      };
      this.habit.emit(habit);
      this.exit.emit();
    }
  }

  dayChange(i: number) {
    this.activeDays[i] = !this.activeDays[i];
  }

  openEmoji() {
    this.emojiModalOpen = true;
  }

  setEmoji(e: string) {
    this.emoji = e;
    this.emojiError = false;
  }

  getInputError(title) {
    this.inputError = this.submitted && !title;
  }
}
