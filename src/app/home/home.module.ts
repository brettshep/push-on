import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";

import { RouterModule, Routes } from "@angular/router";
import { HomeComponent } from "./home.component";
import { EmojiPipe } from './emoji.pipe';
import { NewHabitComponent } from './new-habit/new-habit.component';
import { EmojiKeyboardComponent } from './emoji-keyboard/emoji-keyboard.component';
import { DatesComponent } from './dates/dates.component';
import { CustomDatePipe } from './custom-date.pipe';
import { HabitComponent } from './habit/habit.component';
import { SlideDirective } from './slide.directive';
import { ConfirmComponent } from './confirm/confirm.component';
import { SortPipe } from './sort.pipe';

export const ROUTES: Routes = [
  {
    path: "",
    component: HomeComponent
  },
  { path: "**", redirectTo: "/home" }
];

@NgModule({
  imports: [CommonModule, RouterModule.forChild(ROUTES)],
  declarations: [HomeComponent, EmojiPipe, NewHabitComponent, EmojiKeyboardComponent, DatesComponent, CustomDatePipe, HabitComponent, SlideDirective, ConfirmComponent, SortPipe]
})
export class HomeModule {}
