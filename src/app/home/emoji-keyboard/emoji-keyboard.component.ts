import { Component, ChangeDetectionStrategy,Output, EventEmitter } from '@angular/core';
import search from "@jukben/emoji-search";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: 'emoji-keyboard',
  template: `
      <div class="container">
      <div class="search-box">
        <i class="fas fa-search"></i>
        <input type="text" #input (input)="searchMoji(input.value)" placeholder="Search Icons..."/>
        <i class="fas fa-times" (click)="exit.emit()"></i>
      </div>
      <div class="results">
        <div 
        *ngFor="let result of results;trackBy: trackByFn" 
        [innerHTML]="result | emoji"
        (click)="select(result)"
        >

        </div>
      </div>
      <div class="more">
        <button (click)="showMore()" *ngIf="currNumShown <= allResults.length">See More</button>
      </div>
    </div>
  `,
  styleUrls: ['./emoji-keyboard.component.sass']
})
export class EmojiKeyboardComponent  {
  results: string[];
  allResults : string[];
  maxResults = 30
  currNumShown = this.maxResults;

  @Output() exit = new EventEmitter()
  @Output() emoji = new EventEmitter<string>()


  ngOnInit(){
    this.searchMoji('') 
  }

  select(result:string){
    this.emoji.emit(result)
    this.exit.emit()
  }

  searchMoji(val:string) {
    const res = search(val);
    const set = new Set<string>();
    res.forEach((e) => set.add((e as any).code));
    this.allResults = [...set.values()];
    this.results = this.allResults.slice(0, this.maxResults);
    this.currNumShown = this.maxResults;
  }

  showMore(){
    this.currNumShown += this.maxResults;
    this.results = this.allResults.slice(0, this.currNumShown);
  }

  trackByFn(index, item){
 return item
  }

}
