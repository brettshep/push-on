import { Pipe, PipeTransform } from "@angular/core";
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";

@Pipe({
  name: "emoji",
})
export class EmojiPipe implements PipeTransform {
  constructor(private domSanitizer: DomSanitizer) {}

  transform(emoji: string): SafeHtml {
    const castedCode = twemoji.convert.fromCodePoint(emoji);
    return this.domSanitizer.bypassSecurityTrustHtml(
      twemoji.parse(castedCode, {
        folder: "svg",
        ext: ".svg",
      })
    );
  }
}

declare var twemoji: {
  convert: { fromCodePoint(str: string): string };
  parse(str: string, options?: { folder: string; ext: string }): string;
};
