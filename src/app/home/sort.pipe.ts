import { Pipe, PipeTransform } from "@angular/core";
import { Habit } from "../../../interfaces";

@Pipe({
  name: "sort",
})
export class SortPipe implements PipeTransform {
  transform(habits: Habit[], ...args: unknown[]): unknown {
    return habits.sort((a, b) => a.created - b.created);
  }
}
