import { Component, OnInit, ViewChild, ElementRef } from "@angular/core";
import { HabitService } from "../services/habit.service";
import { Store } from "../store";
import { User, Habit } from "../../../interfaces";
import { BehaviorSubject, Observable } from "rxjs";

@Component({
  selector: "app-home",
  template: `
    <div
      class="container"
      [style.overflowY]="newHabitOpen ? 'hidden' : 'auto'"
      *ngIf="currDate$ | async as currDate"
    >
      <div class="max">
        <h2 class="curr-date-name">{{ currDate | customDate }}</h2>
        <dates
          [dateStart]="startOfWeek$ | async"
          [setDay]="currDate"
          (daySelect)="dayChange($event)"
        ></dates>

        <ng-container *ngIf="habits$ | async as habits; else loading">
          <div class="habits" *ngIf="habits.length; else empty">
            <habit
              *ngFor="let habit of habits | sort"
              [habit]="habit"
              [canCheck]="currDate.getTime() === today.getTime()"
              (toggleActive)="toggleHabit(habit)"
              (delete)="deleteHabit($event)"
            ></habit>
          </div>
        </ng-container>

        <ng-template #empty>
          <div class="empty">
            <h1>No Habits!</h1>
            <img src="/assets/empty.png" alt="empty" />
          </div>
        </ng-template>

        <ng-template #loading>
          <div class="spinner"></div>
        </ng-template>

        <button class="plus" (click)="newHabitOpen = true">
          <i class="fas fa-plus"></i>
        </button>

        <!-- new habit -->
        <new-habit
          *ngIf="newHabitOpen"
          (exit)="newHabitOpen = false"
          (habit)="createHabit($event)"
        ></new-habit>

        <img #award src="/assets/award2.png" alt="award" class="congrats" />
      </div>
    </div>
  `,
  styleUrls: ["./home.component.sass"],
})
export class HomeComponent implements OnInit {
  startOfWeek$: BehaviorSubject<Date>;
  currDate$: BehaviorSubject<Date>;
  habits$: Observable<Habit[]>;
  user$: Observable<User>;
  newHabitOpen: boolean;
  confirmSkipOpen: boolean;
  confirmDeleteOpen: boolean;
  @ViewChild("award")
  awardRef: ElementRef<HTMLDivElement>;

  constructor(private habitServ: HabitService, private store: Store) {}

  ngOnInit() {
    this.user$ = this.store.select<User>("user");
    this.startOfWeek$ = this.habitServ.startOfWeek$;
    this.currDate$ = this.habitServ.currDate$;
    this.habits$ = this.habitServ.habits$;

    this.habitServ.award$.subscribe(() => {
      //show copy popup
      const award = this.awardRef.nativeElement;
      award.classList.remove("active");
      void award.offsetWidth;
      setTimeout(() => {
        award.classList.add("active");
      }, 0);
    });
  }

  createHabit(habit: Habit) {
    this.habitServ.createHabit(habit);
  }

  deleteHabit(habit: Habit) {
    this.habitServ.deleteHabit(habit);
  }

  toggleHabit(habit: Habit) {
    this.habitServ.toggleHabit(habit);
  }

  dayChange(day: number) {
    this.habitServ.dayChange(day);
  }

  get today() {
    return this.habitServ.today;
  }
}
