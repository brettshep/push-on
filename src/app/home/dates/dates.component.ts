import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "dates",
  template: `
    <div class="container">
      <div class="daysCont">
        <!-- <button class="arrow" (click)="move('left')">
        <i class="fal fa-chevron-left"></i>
        </button> -->
        <div class="days">
          <button (click)="dayChange(0)" [class.active]="day === 0">S</button>
          <button (click)="dayChange(1)" [class.active]="day === 1">M</button>
          <button (click)="dayChange(2)" [class.active]="day === 2">T</button>
          <button (click)="dayChange(3)" [class.active]="day === 3">W</button>
          <button (click)="dayChange(4)" [class.active]="day === 4">T</button>
          <button (click)="dayChange(5)" [class.active]="day === 5">F</button>
          <button (click)="dayChange(6)" [class.active]="day === 6">S</button>
        </div>
        <!-- <button class="arrow" (click)="move('right')">
        <i class="fal fa-chevron-right"></i>
        </button> -->
      </div>
    </div>
  `,
  styleUrls: ["./dates.component.sass"],
})
export class DatesComponent {
  start: Date;
  end: Date;
  day: number = 0;

  @Input()
  set dateStart(value) {
    this.start = value;
    this.end = new Date(
      value.getFullYear(),
      value.getMonth(),
      value.getDate() + 6
    );
  }

  @Input()
  set setDay(value: Date) {
    this.day = Math.round((value.getTime() - this.start.getTime()) / 86400000);
  }

  // @Output()
  // dateChange = new EventEmitter<string>();

  @Output()
  daySelect = new EventEmitter<number>();

  // move(event) {
  //   this.dateChange.emit(event);
  // }

  dayChange(event) {
    this.daySelect.emit(event);
  }
}
