import {
  Component,
  ChangeDetectionStrategy,
  Input,
  Output,
  EventEmitter,
} from "@angular/core";
import { Habit } from "../../../../interfaces";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "habit",
  template: `
    <div
      class="container"
      slide
      (swipeLeft)="deleteShow = true"
      (swipeRight)="deleteShow = false"
      [canSlide]="canCheck"
      [class.transparent]="!canCheck"
    >
      <div class="content" [class.deleteShow]="deleteShow">
        <!-- delete -->
        <div class="swipeOptions">
          <button (click)="confirmDeleteOpen = true">
            <i class="fas fa-trash"></i>
          </button>
        </div>
        <!-- emoji -->
        <div class="emoji" [innerHTML]="habit.icon | emoji"></div>
        <!-- text -->
        <div class="text">
          <h3>{{ habit.title }}</h3>
          <p *ngIf="canCheck">Streak: {{ habit.streak }}</p>
        </div>
        <!-- checkbox -->
        <button
          *ngIf="canCheck"
          class="checkbox"
          [class.checked]="
            habit.lastChecked && habit.lastChecked !== habit.prevLastChecked
          "
          (click)="toggleActive.emit()"
        >
          <i class="fas fa-check"></i>
        </button>
      </div>
    </div>

    <!-- confirm box -->
    <confirm
      *ngIf="confirmDeleteOpen"
      [title]="'Permanently Delete Habit?'"
      [button]="'Delete'"
      (accept)="onAccept()"
      (exit)="onCancel()"
    ></confirm>
  `,
  styleUrls: ["./habit.component.sass"],
})
export class HabitComponent {
  confirmDeleteOpen: boolean;

  @Input() habit: Habit;
  @Input() canCheck: boolean;

  @Output() toggleActive = new EventEmitter();
  @Output() delete = new EventEmitter<Habit>();

  onAccept() {
    this.deleteShow = false;
    this.delete.emit(this.habit);
  }

  onCancel() {
    this.confirmDeleteOpen = false;

    this.deleteShow = false;
  }

  deleteShow: boolean = false;
}
