import { EventEmitter } from "@angular/core";
import {
  Component,
  Output,
  Input,
  ChangeDetectionStrategy,
} from "@angular/core";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "confirm",
  template: `
    <div class="bg">
      <div class="modal">
        <h2>{{ title }}</h2>
        <button (click)="onClick()" [class.delete]="button === 'Delete'">
          {{ button }}
        </button>
        <p (click)="exit.emit()">
          Cancel
        </p>
      </div>
    </div>
  `,
  styleUrls: ["./confirm.component.sass"],
})
export class ConfirmComponent {
  @Input() title: string;
  @Input() button: string;

  @Output() accept = new EventEmitter();
  @Output() exit = new EventEmitter();

  onClick() {
    this.accept.emit();
    this.exit.emit();
  }
}
