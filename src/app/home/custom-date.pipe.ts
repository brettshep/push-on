import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'customDate'
})
export class CustomDatePipe implements PipeTransform {

  months = ['January','February','March','April','May',
'June',"July","August", "September", 'October','November','December']

  transform(date: Date): unknown {
    if(this.isToday(date)){
      return "Today"
    }else{
      return `${this.months[date.getMonth()]} ${date.getDate()}`
    }
  }
  

   isToday(someDate: Date){
    const today = new Date()
    return someDate.getDate() == today.getDate() &&
      someDate.getMonth() == today.getMonth() &&
      someDate.getFullYear() == today.getFullYear()
  }

}
