import {
  Component,
  Output,
  EventEmitter,
  ChangeDetectionStrategy,
} from "@angular/core";
import { FormBuilder, FormGroup, Validators } from "@angular/forms";

@Component({
  changeDetection: ChangeDetectionStrategy.OnPush,
  selector: "auth-form",
  styleUrls: ["auth-form.component.sass"],
  template: `
    <div class="auth-form">
      <div class="max">

      
      <div class="top">

        <div class="logo"><img src="/assets/logo.png" alt="logo" /></div>
        <form [formGroup]="form" (ngSubmit)="OnSubmit()">
          <ng-content select="h1"></ng-content>
  
          <label >
            <i class="fas fa-envelope"></i>
            <input
              autocomplete="username"
              type="email"
              formControlName="email"
              placeholder="Email"
            />
          </label>
  
          <label>
            <i class="fas fa-key"></i>
            <input
              autocomplete="current-password"
              type="password"
              formControlName="password"
              placeholder="Password"
            />
          </label>
  
          <div class="error" *ngIf="emailFormat">
            Invalid email format.
          </div>
          <div class="error" *ngIf="passwordInvalid">
            Password is Required.
          </div>
  
          <ng-content select=".error"></ng-content>
  
          <div class="auth-form__button">
            <ng-content select=".actionBtn"></ng-content>
          </div>
  
          <div class="auth-form__toggleLink">
            <ng-content select="a"></ng-content>
          </div>
        </form>
      </div>
      <div class="bottom">
        <img src="/assets/signup.png" alt="signup-image">
      </div>
    </div>
    </div>
  `,
})
export class AuthFormComponent {
  @Output() submitted = new EventEmitter<FormGroup>();

  form = this.fb.group({
    email: ["", Validators.email],
    password: ["", Validators.required],
  });

  constructor(private fb: FormBuilder) {}

  OnSubmit() {
    if (this.form.valid) {
      //emit
      this.submitted.emit(this.form);
    }
  }

  get passwordInvalid() {
    const control = this.form.get("password");
    return control.hasError("required") && control.touched;
  }

  get emailFormat() {
    const control = this.form.get("email");
    return control.hasError("email") && control.touched;
  }
}
