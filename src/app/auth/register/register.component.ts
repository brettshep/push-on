import { Component } from "@angular/core";
import { AuthService } from "../../services/auth.service";
import { Router } from "@angular/router";
import { FormGroup } from "@angular/forms";

@Component({
  selector: "app-register",
  template: `
    <div style="height: 100%">
      <auth-form (submitted)="RegisterUser($event)">
        <h1>Track your Habits.</h1>
        <div class="error" *ngIf="error">
          {{ error }}
        </div>
        <button class="actionBtn" type="submit">Sign Up</button>
        <a routerLink="/auth/login">Have An Account?</a>
      </auth-form>
    </div>
  `,
  styleUrls: ["./register.component.sass"],
})
export class RegisterComponent {
  constructor(private authServ: AuthService, private router: Router) {}

  error: string;

  async RegisterUser(event: FormGroup) {
    const { email, password } = event.value;
    try {
      await this.authServ.createUser(email, password);
      this.router.navigate(["/planner"]);
    } catch (err) {
      this.error = err.message;
    }
  }
}
