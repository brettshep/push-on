import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
// import { HttpClientModule, HttpClientJsonpModule } from "@angular/common/http";
import { AppRoutingModule } from "./routing.module";
import { AppComponent } from "./app.component";
import { BrowserAnimationsModule } from "@angular/platform-browser/animations";
import { Store } from "./store";

// Firebase
import { AngularFireModule } from "@angular/fire";
import { AngularFireAuthModule } from "@angular/fire/auth";
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

const config = {
  apiKey: "AIzaSyCYGf65f_fxlG8OLR-oSJ7W1B3_q-tKRDo",
  authDomain: "push-on-bbc97.firebaseapp.com",
  databaseURL: "https://push-on-bbc97.firebaseio.com",
  projectId: "push-on-bbc97",
  storageBucket: "push-on-bbc97.appspot.com",
  messagingSenderId: "839051703511",
  appId: "1:839051703511:web:b4c0c128c488526ad96c92",
  measurementId: "G-CCE9JBTREG",
};

@NgModule({
  declarations: [AppComponent],
  imports: [
    BrowserModule,
    AppRoutingModule,
    // HttpClientModule,
    // HttpClientJsonpModule,
    BrowserAnimationsModule,
    AngularFireModule.initializeApp(config),
    AngularFireAuthModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  providers: [Store],
  bootstrap: [AppComponent],
})
export class AppModule {}
