import { Injectable, EventEmitter } from "@angular/core";
import { AngularFirestore } from "@angular/fire/firestore";
import { Store } from "../store";
import { AuthService } from "./auth.service";
import { BehaviorSubject, Observable, Subscription } from "rxjs";
import { Habit, User } from "../../../interfaces";
import { switchMap } from "rxjs/operators";

@Injectable({
  providedIn: "root",
})
export class HabitService {
  startOfWeek$: BehaviorSubject<Date>;
  currDate$: BehaviorSubject<Date>;
  habits$: Observable<Habit[]>;
  award$ = new EventEmitter();
  private startedPoll: boolean;

  constructor(
    private fireStore: AngularFirestore,
    private store: Store,
    private authServ: AuthService
  ) {
    this.startOfWeek$ = new BehaviorSubject(this.getStartOfWeek(this.today));
    this.currDate$ = new BehaviorSubject(this.today);
    this.habits$ = this.currDate$.pipe(
      switchMap((date: Date) => {
        const dayOfWeek = date.getDay().toString();
        return this.getHabits(date.getTime().toString(), dayOfWeek);
      })
    );
  }

  getHabits(dateTime: string, dayOfWeek: string): Observable<Habit[]> {
    const storeVal = this.store.getDirect(dateTime);

    if (!storeVal) {
      this.fireStore
        .collection("users")
        .doc(this.UID)
        .collection("habits", (ref) =>
          ref.where("days", "array-contains", dayOfWeek)
        )
        .get()
        .toPromise()
        .then((query) => {
          let habits: Habit[] = [];
          if (!query.empty) {
            habits = query.docs.map((doc) => doc.data() as Habit);
          }
          this.store.set(dateTime, habits);

          // start poll check when first loading todays
          if (!this.startedPoll) {
            this.checkDailyUpdateHabits();
            this.startedPoll = true;
          }
        });
    }

    return this.store.select<Habit[]>(dateTime);
  }

  createHabit(habit: Habit) {
    const uid = this.fireStore.createId();
    habit.uid = uid;

    // create in store
    const dayTimesOfWeek = this.thisWeeksDateTimes;

    // loop through days (['1','4'] etc.) of habit
    habit.days.forEach((day) => {
      // get date time millis (1591588800000) from day
      const dayTime = dayTimesOfWeek[parseInt(day)];
      const daysHabits = this.store.getDirect<Habit[]>(dayTime);

      // if day is in store, add it
      if (daysHabits) {
        this.store.set(dayTime, [...daysHabits, habit]);
      }
    });

    // set in db
    this.fireStore
      .collection("users")
      .doc(this.UID)
      .collection("habits")
      .doc(uid)
      .set(habit);
  }

  deleteHabit(habit: Habit) {
    // delete in store
    const dayTimesOfWeek = this.thisWeeksDateTimes;

    // loop through days (['1','4'] etc.) of habit
    habit.days.forEach((day) => {
      // get date time millis (1591588800000) from day
      const dayTime = dayTimesOfWeek[parseInt(day)];
      const daysHabits = this.store.getDirect<Habit[]>(dayTime);

      // if day is in store, remove it
      if (daysHabits) {
        const newDaysHabits = daysHabits.filter(
          (_habit) => _habit.uid !== habit.uid
        );
        this.store.set(dayTime, newDaysHabits);
      }
    });

    // remove in db
    this.fireStore
      .collection("users")
      .doc(this.UID)
      .collection("habits")
      .doc(habit.uid)
      .delete();
  }

  updateHabit(habit: Habit) {
    // update in store
    const dayTimesOfWeek = this.thisWeeksDateTimes;

    // loop through days (['1','4'] etc.) of habit
    habit.days.forEach((day) => {
      // get date time millis (1591588800000) from day
      const dayTime = dayTimesOfWeek[parseInt(day)];
      const daysHabits = this.store.getDirect<Habit[]>(dayTime);

      // if day is in store, update it
      if (daysHabits) {
        const habitIndex = daysHabits.findIndex(
          (_habit) => _habit.uid === habit.uid
        );
        daysHabits[habitIndex] = { ...habit };
        this.store.set(dayTime, [...daysHabits]);
      }
    });

    // update in db

    this.fireStore
      .collection("users")
      .doc(this.UID)
      .collection("habits")
      .doc(habit.uid)
      .update(habit);
  }

  toggleHabit(habit: Habit) {
    // assess 'checked status based on lastChecked and prevChecked'
    let checked;
    if (habit.lastChecked === null) {
      checked = true;
    } else {
      habit.lastChecked === habit.prevLastChecked
        ? (checked = true)
        : (checked = false);
    }

    // create new habit
    let newHabit: Habit = { ...habit };
    if (checked) {
      newHabit.lastChecked = this.today.getTime();
      newHabit.streak++;
    } else {
      newHabit.lastChecked = newHabit.prevLastChecked;
      newHabit.streak--;
    }

    this.updateHabit(newHabit);

    // see if award for all habits completed today
    let allComplete = true;
    const habits = this.store.getDirect<Habit[]>(
      this.today.getTime().toString()
    );
    for (let i = 0; i < habits.length; i++) {
      const habit = habits[i];
      if (!habit.lastChecked || habit.lastChecked === habit.prevLastChecked) {
        allComplete = false;
        break;
      }
    }
    if (allComplete) this.award$.emit();
  }

  checkDailyUpdateHabits() {
    const user = this.store.getDirect<User>("user");
    if (!user) return;
    const today = this.today.getTime();
    const lastDayUpdated = user.lastDayUpdated;

    if (today - lastDayUpdated >= 86400000) {
      // update week
      const newStartOfWeek = this.getStartOfWeek(this.today);
      this.startOfWeek$.next(newStartOfWeek);

      // update currday
      this.dayChange(this.today.getDay());

      // update last updated
      user.lastDayUpdated = today;
      this.fireStore.collection("users").doc(this.UID).update(user);

      this.updateTodaysStreaks();
    }
    setTimeout(this.checkDailyUpdateHabits.bind(this), 5000);
  }

  updateTodaysStreaks() {
    const today: Date = this.today;
    const currDayTime: number = today.getTime();
    const currDay: number = today.getDay();
    const habits$ = this.store.select<Habit[]>(currDayTime.toString());

    // sub to store to wait for data
    const sub = habits$.subscribe((habits) => {
      // make sure data has actually been set
      if (!Array.isArray(habits)) return;
      setTimeout(() => {
        sub.unsubscribe();
        if (habits) {
          habits.forEach((habit) => {
            let closestPrevDayAwayTime: number;
            let index = currDay;
            let count = 0;
            for (let i = 0; i < 7; i++) {
              count++;
              index--;
              if (index < 0) index = 6;
              if (habit.days.includes(index.toString())) {
                closestPrevDayAwayTime = count * 86400000;
                break;
              }
            }
            // habit wasnt completed in day in was supposed to be
            if (
              !habit.lastChecked ||
              habit.lastChecked < this.today.getTime() - closestPrevDayAwayTime
            ) {
              habit.streak = 0;
            }
            habit.prevLastChecked = habit.lastChecked;
            habit.lastChecked = null;
            // update habit in store and db
            this.updateHabit(habit);
          });
        }
      }, 0);
    });
  }

  /* #region  --Date Changes */
  private getStartOfWeek(date: Date) {
    const day = date.getDay();
    const diff = date.getDate() - day;
    date.setDate(diff);
    date.setHours(0, 0, 0, 0);
    return date;
  }

  private getInitalDate(date: Date) {
    date.setHours(0, 0, 0, 0);
    return date;
  }

  dayChange(day: number) {
    const oldDate = this.startOfWeek$.getValue();
    const newDate = new Date(
      oldDate.getFullYear(),
      oldDate.getMonth(),
      oldDate.getDate() + day
    );
    this.currDate$.next(newDate);
  }
  /* #endregion */

  get today() {
    let date = new Date();
    date.setDate(date.getDate());
    return this.getInitalDate(date);
  }

  get UID(): string {
    return this.authServ.UID;
  }

  get thisWeeksDateTimes(): string[] {
    const weekStartTime = this.startOfWeek$.value.getTime();
    let dates: string[] = [];
    for (let i = 0; i < 7; i++) {
      dates.push((weekStartTime + i * 86400000).toString());
    }
    return dates;
  }
}
