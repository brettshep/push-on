export interface User {
  uid: string;
  email: string;
  lastDayUpdated: number;
}

export interface Habit {
  uid?: string;
  title: string;
  icon: string;
  days: string[];
  lastChecked: number;
  prevLastChecked: number;
  streak: number;
  created: number;
}
